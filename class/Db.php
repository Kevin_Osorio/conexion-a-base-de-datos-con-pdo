<?php

require_once('../const.php');

/**
 * Clase para conexion de la base de datos
 */
class Db{
    protected $dbUser = DB_USER;
    protected $dbPassword = DB_PASSWORD;
    protected $dbName = DB_NAME;
    protected $dbHost = DB_HOST;
    protected $con;
    
    /**
     * __construct conexion a la base de datos
     * @author Wilson Ramirez Z
     * @return void
     */
    public function __construct()
    {
        try{
            $this->con = new PDO("mysql:host=$this->dbHost;dbname=$this->dbName;charset=utf8", $this->dbUser, $this->dbPassword);
            $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            echo 'Error: ' . $e->getMessage();
        }
    }
   
}
